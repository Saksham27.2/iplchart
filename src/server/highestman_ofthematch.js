const fileConvert=require("../server/convert")

function highestman(marr) {
    const ans = {};
  
    for (let i = 0; i < marr.length; i++) {
        const data = marr[i];
        const player = data.player_of_match;
        const season = data.season;

        if (!ans[season]) {
            ans[season] = {};
        }

        if (!ans[season][player]) {
            ans[season][player] = 1;
        }

        ans[season][player]++;
    }

    // For Sorting and printing the top results  
    const result = {};
    for (let season in ans) {
        const players = ans[season];
        let maxCount = 0;
        let playerWithCount = null;

        for (let player in players) {
            if (players[player] > maxCount) {
                maxCount = players[player];
                playerWithCount = player;
            }
        }

        if (playerWithCount) {
            result[season] = { player: playerWithCount, count: maxCount };
        }
    }

    fileConvert.write("highestman_ofthematch",result)
}



fileConvert.getSingleData("matches",highestman)
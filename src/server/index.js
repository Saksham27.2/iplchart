const express=require("express")
const path=require('path')
const app=express();
const Port=process.env.PORT || 4000;




app.use(express.static(path.join(__dirname,"..",'public')));


app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname, '..', 'public','index.html'));
});

app.listen(Port,()=>{
    console.log(`Server started on ${Port}`);
    
})


document.addEventListener('DOMContentLoaded', async function () {
    // Function to fetch and visualize data
    async function fetchDataAndVisualize(jsonFilePath, chartContainerId, chartTitle) {
      try {
        const response = await fetch(jsonFilePath);
        const data = await response.json();
        console.log('HELLO FROM IPL.JS');
        // Process the data and visualize it using Highcharts
        Highcharts.chart(chartContainerId, {
          title: {
            text: chartTitle
          },
          xAxis: {
            categories: Object.keys(data)
          },
          yAxis: {
            title: {
              text: 'Number of Matches'
            }
          },
          series: [{
            name: 'Matches',
            data: Object.values(data)
          }]
        });
      } catch (error) {
        console.error('Error fetching JSON:', error);
      }
    }
  
    // Call the function with specific JSON files and chart containers
    // await fetchDataAndVisualize('/home/saksham/Js/Mblue/js-ipl-data-project/src/public/output/matchesperyear.json', 'container1', 'Matches Per Year');
    // await fetchDataAndVisualize('public/output/teamwonntoss.json', 'container2', 'Team wonn toss and match');
  });
  
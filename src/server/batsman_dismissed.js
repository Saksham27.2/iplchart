const fileConvert=require("../server/convert")

    
const batsmandismissed = (dpath) => {
    const ans = {};
  
    for (let i = 0; i < dpath.length; i++) {
      const data = dpath[i];
      const batsman = data.player_dismissed;
      const bowler = data.bowler;
  
      if (batsman !== '') {
        const pairkey = `${bowler} - ${batsman}`;
        ans[pairkey] = (ans[pairkey] || 0) + 1;
      }
    }
  
    let highestDismissalsPair = null;
    let highestDismissalsCount = 0;
  
    for (let pairKey in ans) {
      const count = ans[pairKey];
      if (count > highestDismissalsCount) {
        highestDismissalsCount = count;
        highestDismissalsPair = pairKey;
      }
    }
  
   const res={ highestDismissalsPair, highestDismissalsCount };
   fileConvert.write("batsman_dismissed",res)
  };
  


fileConvert.getSingleData("deliveries",batsmandismissed)